package yh.datastructures

/**
  * Created by hoey on 12/12/15.
  */
object Play extends App {

  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean =
    sub.forall(sup.contains(_))

  println(hasSubsequence(List(1, 2, 3, 4), List(1)))
  println(hasSubsequence(List(1, 2, 3, 4), List(1, 5)))
  println(hasSubsequence(List(1, 2, 3, 4), List(1, 2, 4)))

  println(Branch(Leaf(1), Leaf(2)).size)

}
