package yh.datastructures

sealed trait Tree[+A] {
  def size: Int = this match {
    case Leaf(_) => 1
    case Branch(l, r) => l.size + r.size + 1
  }
}
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {
  def maximum(t: Tree[Int]): Int = t match {
    case Leaf(v) => v
    case Branch(l, r) => maximum(l) max maximum(r)
  }

  def fold[A, B](t: Tree[A], z: B)(f: (A, B) => B): B = t match {
    case Leaf(v) => f(v, z)
    case Branch(l, r) => fold(r, fold(l, z)(f))(f)
  }
}
