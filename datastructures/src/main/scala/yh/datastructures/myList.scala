package yh.datastructures

sealed trait MyList[+A]

case object Nil extends MyList[Nothing] {
  override def toString = "$"
}

case class Cons[+A](head: A, tail: MyList[A]) extends MyList[A] {
  override def toString = head.toString + "::" + tail.toString
}

object MyList {
  def sum(ints: MyList[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def product(ds: MyList[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  // Exercise 3.2
  def tail[A](l: MyList[A]): MyList[A] = l match {
    case Cons(_, t) => t
    case Nil => sys.error("Nil.tail")
  }

  // Exercise 3.3
  def setHead[A](l: MyList[A], h: A): MyList[A] = l match {
    case Cons(_, t) => Cons(h, t)
    case Nil => sys.error("Nil.setHead")
  }

  // Exercise 3.4
  def drop[A](l: MyList[A], n: Int): MyList[A] = l match {
    case Cons(_, t) if n > 0 => drop(t, n - 1)
    case _ => l
  }

  // Exercise 3.5
  def dropWhile[A](l: MyList[A], f: A => Boolean): MyList[A] = l match {
    case Cons(h, t) if f(h) => dropWhile(t, f)
    case _ => l
  }

  // Exercise 3.6
  def init[A](l: MyList[A]): MyList[A] = l match {
    case Nil => sys.error("Nil.init")
    case Cons(_, Nil) => Nil
    case Cons(h, t) => Cons(h, init(t))
  }

  def foldRight[A, B](l: MyList[A], z: B)(f: (A, B) => B): B = l match {
    case Cons(h, t) => f(h, foldRight(t, z)(f))
    case Nil => z
  }

  def product2(ds: MyList[Double]): Double =
    foldRight(ds, 1.0)(_ * _)

  // Exercise 3.9
  def length[A](as: MyList[A]): Int =
    foldRight(as, 0)((_, b) => b + 1)

  // Exercise 3.10
  @annotation.tailrec
  def foldLeft[A, B](as: MyList[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(h, t) => foldLeft(t, f(z, h))(f)
  }

  // Exercise 3.11
  def sum2(as: MyList[Int]): Int =
    foldLeft(as, 0)(_ + _)

  // Exercise 3.12
  def reverse[A](as: MyList[A]): MyList[A] =
    foldLeft(as, Nil: MyList[A])((b, a) => Cons(a, b))

  // Exercise 3.13
  def foldRight2[A, B](as: MyList[A], z: B)(f: (A, B) => B): B = {
    foldLeft(as, (b: B) => b)((g, a) => (b: B) => g(f(a, b)))(z)
  }
  def foldLeft2[A, B](as: MyList[A], z: B)(f: (B, A) => B): B = {
    foldRight(as, (b: B) => b)((a, g) => (zz: B) => g(f(zz, a)))(z)
  }

  // Exercise 3.14
  def append[A](a1: MyList[A], a2: MyList[A]): MyList[A] =
    foldRight(a1, a2)(Cons(_, _))

  // Exercise 3.15
  def concat[A](ass: MyList[MyList[A]]): MyList[A] =
    foldRight(ass, Nil: MyList[A])((a, b) => append(a, b))

  def apply[A](as: A*): MyList[A] = {
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))
  }

  def add1(ns: MyList[Int]): MyList[Int] =
    foldRight(ns, Nil: MyList[Int])((a, b) => Cons(a + 1, b))

  def dToS(ds: MyList[Double]): MyList[String] =
    foldRight(ds, Nil: MyList[String])((h, t) => Cons(h.toString, t))

  def map[A, B](as: MyList[A], f: A => B): MyList[B] = {
    foldRight(as, Nil: MyList[B])((a, t) => Cons(f(a), t))
  }

  def filter[A](as: MyList[A], f: A => Boolean): MyList[A] =
    foldRight(as, Nil: MyList[A])((a, b) => if (f(a)) Cons(a, b) else b)

  def flatMap[A, B](as: MyList[A], f: A => MyList[B]): MyList[B] =
    concat(map(as, f))

  def filter2[A](as: MyList[A], f: A => Boolean): MyList[A] =
    flatMap(as, (a: A) => if (f(a)) Cons(a, Nil) else Nil)

  def add2(as: MyList[Int], bs: MyList[Int]): MyList[Int] = (as, bs) match {
    case (Cons(ah, at), Cons(bh, bt)) => Cons(ah + bh, add2(at, bt))
    case _ => Nil
  }

  def zipWith[A, B, C](as: MyList[A], bs: MyList[B])(f: (A, B) => C): MyList[C] = (as, bs) match {
    case (Cons(ah, at), Cons(bh, bt)) => Cons(f(ah, bh), zipWith(at, bt)(f))
    case _ => Nil
  }

}