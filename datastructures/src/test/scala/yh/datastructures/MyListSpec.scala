package yh.datastructures

import org.scalacheck.Gen._
import org.scalacheck._
import org.specs2._

class MyListSpec extends Specification with ScalaCheck { def is = s2"""

Same as scala standard List implementations
-------------------------------------------
  $sum
  $product
  $drop
  $dropWhile
  $init
  $product2
  $length
  $sum2
  $reverse
  $foldRight2
  $foldLeft2
  $concat

Unit Tests
----------
  MyList.tail must return a list without its head
    ${ MyList.tail(Seq(1)) must_== Nil }
    ${ MyList.tail(Seq(1, 2)) must_== MyList(2)}

  MyList.tail causes an error if it's Nil
    ${ MyList.tail(Nil) must throwA[RuntimeException](message = "Nil.tail") }

  MyList.setHead changes its head
    ${ MyList.setHead(Seq(1), 2) must_== MyList(2) }
    ${ MyList.setHead(Seq(1, 2), 3) must_== MyList(3, 2) }

  MyList.setHead causes an error if it's Nil
    ${ MyList.setHead(Nil, 1) must throwA[RuntimeException](message = "Nil.setHead")}

  MyList.drop removes the first n elements from the list.
    ${ MyList.drop(Seq(1), 1) must_== Nil }
    ${ MyList.drop(Seq(1, 2), 1) must_== MyList(2) }

  MyList.init throws an exception when it's Nil
    ${ MyList.init(MyList()) must throwA[RuntimeException] }

"""

  implicit def toMyList[T](l: Seq[T]): MyList[T] = MyList(l: _*)

  implicit def DoubleArbitrary: Arbitrary[Double] = Arbitrary(choose(-10.0, 10.0))

  implicit def ListDoubleArbitrary: Arbitrary[List[Double]] =
    Arbitrary(for {
      n <- choose(0, 10)
      l <- listOfN(n, DoubleArbitrary.arbitrary)
    } yield l)

  def ==~(d: =>Double) = beCloseTo(d +/- 0.001)

  def sum = prop { l: List[Int] => MyList.sum(l) must_== l.sum }.collect

  def product = prop { l: List[Double] =>
    MyList.product(l) must ==~ (l.product)
  }

  def drop = prop { (l: List[Int], n: Int) =>
    MyList.drop(l, n) must_== MyList(l.drop(n): _*)
  }

  def dropWhile = prop { (l: List[Int], a: Int) =>
    val f: Int => Boolean = { x => x < a }
    MyList.dropWhile(l, f) must_== MyList(l.dropWhile(f): _*)
  }

  def init = prop { l: List[Int] =>
    l.nonEmpty ==> { MyList.init(l) must_== MyList(l.init: _*) }
  }

  def product2 = prop { l: List[Double] =>
    MyList.product2(l) must ==~ (MyList.product(l))
  }

  def length = prop { l: List[Int] =>
    MyList.length(l) must_== l.length
  }

  def sum2 = prop { l: List[Int] =>
    MyList.sum2(l) must_== l.sum
  }

  def reverse = prop { l: List[Int] =>
    MyList.reverse(l) must_== MyList(l.reverse: _*)
  }

  def foldRight2 = prop { l: List[String] =>
    val addLen = (a: String, b: Int) => a.length + b
    MyList.foldRight2(l, 0)(addLen) must_== MyList.foldRight(l, 0)(addLen)
  }

  def foldLeft2 = prop { l: List[String] =>
    val addLen = (b: Int, a: String) => a.length + b
    MyList.foldLeft2(l, 0)(addLen) must_== MyList.foldLeft(l, 0)(addLen)
  }

  def concat = prop { ls: List[List[Int]] =>
    val mls = MyList(ls.map(toMyList): _*)
    MyList.concat(mls) must_== MyList(ls.flatten: _*)
  }
}