package yh.parallelism

import java.util.concurrent.{Callable, TimeUnit, Future, ExecutorService}

object Par {
  type Par[A] = ExecutorService => Future[A]

  def unit[A](a: A): Par[A] = (es: ExecutorService) => UnitFuture(a)

  def lazyUnit[A](a: A): Par[A] = fork(unit(a))

  private case class UnitFuture[A](get: A) extends Future[A] {
    def isDone = true
    def get(timeout: Long, units: TimeUnit) = get
    def isCancelled = false
    def cancel(evenIfRunning: Boolean): Boolean = false
  }

  def map2[A,B,C](a: Par[A], b: Par[B])(f: (A,B) => C): Par[C] =
    (es: ExecutorService) => {
      val af = a(es)
      val bf = b(es)
      es.submit(new Callable[C] {
        def call = f(af.get, bf.get)
      })
    }

  def flatMap[A,B](a: Par[A])(f: A => Par[B]): Par[B] =
    (es: ExecutorService) => {
      val af = a(es)

      es.submit(new Callable[B] {
        def call = f(af.get)(es).get
      })
    }

  def map3[A,B,C,D](pa: Par[A], pb: Par[B], pc: Par[C])(f: (A,B,C) => D): Par[D] = {
    flatMap(pa) { a =>
      flatMap(pb) { b =>
        map(pc) { c =>
          f(a,b,c)
        }
      }
    }
  }

  def map[A,B](a: Par[A])(f: A => B): Par[B] =
    (es: ExecutorService) => {
      val af = a(es)
      UnitFuture(f(af.get))
    }

  def fork[A](a: => Par[A]): Par[A] =
    es => es.submit(new Callable[A] {
      def call = a(es).get
    })

  def asyncF[A,B](f: A => B): A => Par[B] = (a: A) =>
    lazyUnit(f(a))


  def sequence[A](ps: List[Par[A]]): Par[List[A]] = {
    ps.foldRight(unit(List.empty[A]))((a, b) => map2(a, b)(_ :: _))
  }

  def parMap[A,B](ps: List[A])(f: A => B): Par[List[B]] = fork {
    val fbs: List[Par[B]] = ps.map(asyncF(f))
    sequence(fbs)
  }

  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = {
    val fbs: List[Par[List[A]]] = as.map(asyncF(a => if (f(a)) List(a) else List()))
    map(sequence(fbs))(_.flatten)
  }
}