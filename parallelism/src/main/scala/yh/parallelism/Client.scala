package yh.parallelism

object Client {
  import yh.parallelism.Par.Par
  def parMax(numbers: IndexedSeq[Int]): Par[Int] = {
    if (numbers.isEmpty) {
      Par.unit(Int.MinValue)
    } else {

      val (f, s) = numbers.splitAt(numbers.length / 2)
      Par.map2(
        Par.fork(parMax(f)),
        Par.fork(parMax(s)))(_ max _)
    }
  }

//  def countWords(paragraph: String): Par[Int] = {
//    Par.fork(Par.unit(paragraph.split("\s").length))
//  }

  def countWords(ps: List[String]): Par[Int] = {
    Par.unit(0)
  }
}
