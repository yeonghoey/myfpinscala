import yh.laziness._

val s1 = Stream(1, 2, 3)
val s2 = Stream(4, 5, 6)

s1.append(s2).toList

val all = s1.flatMap(n => Stream((1 to n).toList: _*))
all.toList

Stream.ones.take(5).toList
Stream.constant(2).take(5).toList
Stream.from(5).take(3).toList
Stream.fibs2.take(10).toList
Stream.constant2(7).take(10).toList
Stream.from2(888).take(10).toList

Stream(1, 2, 3).zipAll(Stream(4)).toList
Stream(1, 2, 3).startsWith(Stream(1, 2))
Stream(1, 3).startsWith(Stream(1, 2))
Stream(1, 3).startsWith(Stream(1))
Stream(1, 2, 3).scanRight(0)(_ + _).toList