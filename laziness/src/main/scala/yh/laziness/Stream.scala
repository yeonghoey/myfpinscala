package yh.laziness

sealed trait Stream[+A] {

  import Stream._

  // exercise 5.1
  def toList: List[A] = this match {
    case Cons(fh, ft) => fh() :: ft().toList
    case Empty => Nil
  }

  // exercise 5.2
  def take(n: Int): Stream[A] = this match {
    case Cons(fh, ft) if n > 0 => cons(fh(), ft().take(n - 1))
    case _ => Empty
  }

  def drop(n: Int): Stream[A] = this match {
    case Cons(fh, ft) if n > 0 => ft().drop(n - 1)
    case _ => this
  }

  // exercise 5.3
  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Cons(fh, ft) if p(fh()) => cons(fh(), ft().takeWhile(p))
    case _ => Empty
  }

  def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
    case Cons(fh, ft) => f(fh(), ft().foldRight(z)(f))
    case _ => z
  }

  // Not Lazy!!
  def foldLeft[B](z: => B)(f: (=> B, A) => B): B = this match {
    case Cons(fh, ft) => ft().foldLeft(f(z, fh()))(f)
    case _ => z
  }

  // exercise 5.4
  def forAll(p: A => Boolean): Boolean = {
    foldLeft(true)((b, a) => b && p(a))
  }

  // exercise 5.5
  def takeWhile2(p: A => Boolean): Stream[A] =
    foldRight(empty[A])((a, b) => if (p(a)) cons(a, b) else b)

  // exercise 5.6
  def headOption: Option[A] = foldRight(None: Option[A])((h, _) => Some(h))

  // exercise 5.7
  def map[B](f: A => B): Stream[B] =
    foldRight(empty[B])((h, t) => cons(f(h), t))

  def filter(p: A => Boolean): Stream[A] =
    foldRight(empty[A])((h, t) => if (p(h)) cons(h, t) else t)

  def append[B >: A](that: => Stream[B]): Stream[B] =
    foldRight(that)((h, t) => cons(h, t))

  def flatMap[B](f: A => Stream[B]): Stream[B] =
    foldRight(empty[B])((a, b) => f(a).append(b))

  // exercise 5.13
  def map2[B](f: A => B): Stream[B] =
    unfold(this)(s => s.headOption map (h => Some(f(h), s.drop(1))) getOrElse None)

  def take2(n: Int): Stream[A] = {
    unfold((this, n)) {
      case (s, r) =>
        if (n > 0) s.headOption map (Some(_, (s.drop(1), n - 1))) getOrElse None
        else None
    }
  }

  def takeWhile3(p: A => Boolean): Stream[A] = {
    unfold(this)(s =>
      s.headOption map (
        h => if (p(h)) Some(h, s.drop(1)) else None)
        getOrElse None)
  }

  def zipWith[B, C](that: Stream[B])(f: (A, B) => C): Stream[C] = {
    unfold((this, that)) { case (as, bs) =>
      val oc = for {
        a <- as.headOption
        b <- bs.headOption
      } yield f(a, b)

      oc map (c => Some(c, (as.drop(1), bs.drop(1)))) getOrElse None
    }
  }

  def zipAll[B](that: Stream[B]): Stream[(Option[A], Option[B])] = {
    unfold((this, that)) { case (as, bs) =>
      (as.headOption, bs.headOption) match {
        case (oa@Some(_), ob@Some(_)) =>
          Some((oa, ob), (as.drop(1), bs.drop(1)))

        case (oa@Some(_), None) =>
          Some((oa, None), (as.drop(1), empty[B]))

        case (None, ob@Some(_)) =>
          Some((None, ob), (empty[A], bs.drop(1)))

        case _ => None
      }
    }
  }

  // exercise 5.14
  def startsWith[B](that: Stream[B]): Boolean = {
    zipAll(that).filter(_._2.isDefined).forAll {
      case (h1, h2) =>h1 == h2
    }
  }

  // exercise 5.15
  def tails: Stream[Stream[A]] = {
    unfold(this) {
      case Cons(h, t) => Some(Cons(h, t), t())
      case _ => None
    } append Stream(empty)
  }

  // exercise 5.16
  def scanRight[B](z: => B)(f: (A, => B) => B): Stream[B] = {
    val pair = foldRight((z, Stream(z))) { case (a, (b, s)) =>
      lazy val v = f(a, b)
      (v, Stream(v).append(s))
    }
    pair._2
  }
}
case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](head: => A, tail: => Stream[A]): Stream[A] = {
    lazy val hd = head
    lazy val tl = tail
    Cons(() => hd, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

  val ones: Stream[Int] = Stream.cons(1, ones)

  // exercise 5.8
  def constant[A](a: A): Stream[A] = {
    lazy val s: Stream[A] = cons(a, s)
    s
  }

  // exercise 5.9
  def from(n: Int): Stream[Int] = {
    cons(n, from(n+1))
  }

  // exercise 5.10
  def fibs: Stream[Int] = {
    def go(a: Int, b: Int): Stream[Int] =
      cons(a, go(b, a + b))
    go(0, 1)
  }

  // exercise 5.11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = {
    f(z).map { case (a, s) => cons(a, unfold(s)(f)) } getOrElse empty[A]
  }

  // exercise 5.12
  def fibs2: Stream[Int] =
    unfold((0, 1)) { case (a, b) => Some(a, (b, a + b)) }

  def from2(n: Int): Stream[Int] =
    unfold(n)(s => Some(s, s + 1))

  def constant2(n: Int): Stream[Int] =
    unfold(n)(x => Some(x, x))
}