package yh.state

sealed trait Input
case object Coin extends Input
case object Turn extends Input

case class Machine(locked: Boolean, candies: Int, coins: Int) {
  def update(input: Input) = input match {
    case Coin if locked && candies > 0 =>
      Machine(locked = false, candies, coins + 1)
    case Turn if !locked =>
      Machine(locked = true, candies - 1, coins)
    case _ => this
  }
}

object Machine {
  def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = {
    import State._
    val updates = inputs.map { i => State[Machine, Unit](m => ((), m.update(i))) }

    for {
      _ <- sequence(updates) // pick x, transit state.
      s <- get
    } yield (s.coins, s.candies)
  }
}