import yh.state._
val r = SimpleRNG(124)
RNG.double(r)

val sim = Machine.simulateMachine(Coin :: Turn :: Nil)
sim.run(Machine(locked = false, 10, 5))