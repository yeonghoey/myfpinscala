import yh.errorhandling._
Either.sequence(List(Right("ok"), Right("yes")))
Either.sequence(List(Right("ok"), Left("NO!!!"), Right("yes")))
Either.traverse(List("a", "b", "c"))(s => Right(s + "x"))
Either.traverse(List("a", "b", "c"))(s => if (s == "b") Left(10) else Right(s + "x"))


Etr.sequence(List(Lt(1), Lt(2), Rt("a")))
Etr.sequence(List(Rt(1), Rt(2), Rt("a")))
Etr.sequence(List(Rt(1), Lt(2), Rt("a")))