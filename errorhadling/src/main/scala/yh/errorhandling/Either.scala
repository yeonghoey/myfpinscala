package yh.errorhandling

trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B] = this match {
    case Left(e) => Left(e)
    case Right(a) => Right(f(a))
  }

  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] = this match {
    case Left(e) => Left(e)
    case Right(a) => f(a)
  }

  def orElse[EE >: E,B >: A](b: => Either[EE, B]): Either[EE, B] = this match {
    case Right(a) => this
    case _ => b
  }

  // Exercise 4.6
  def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] =
    flatMap(a => b.map(b => f(a, b)))

}

object Either {
  // Exercise 4.7
  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] = es match {
    case Nil => Right(Nil)
    case x :: xs => x flatMap (xx => sequence(xs) map (xxs => xx :: xxs))
  }

  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = {
    as.foldRight[Either[E, List[B]]](Right(Nil))((a, bs) => f(a).map2(bs)(_ :: _))
  }
}

case class Left[+E](value: E) extends Either[E, Nothing]
case class Right[+A](value: A) extends Either[Nothing, A]

// Exercise 4.8
trait Etr[+E, +A] {
  def map[B](f: A => B): Etr[E, B] = this match {
    case Lt(e) => Lt(e)
    case Rt(a) => Rt(f(a))
  }

  def map2[EE >: E, B, C](b: Etr[List[EE], B])(f: (A, B) => C): Etr[List[EE], C] = (this, b) match {
    case (Lt(aa), Rt(_)) => Lt(List(aa))
    case (Rt(_), Lt(es)) => Lt(es)
    case (Lt(e), Lt(es)) => Lt(e :: es)
    case (Rt(aa), Rt(bb)) => Rt(f(aa, bb))
  }
}

object Etr {
  def traverse[E, A, B](as: List[A])(f: A => Etr[E, B]): Etr[List[E], List[B]] =
    as.foldRight[Etr[List[E], List[B]]](Rt(Nil))((a, bs) => f(a).map2(bs)(_ :: _))

  def sequence[E, A](as: List[Etr[E, A]]): Etr[List[E], List[A]] =
    traverse(as)(a => a.map(x => x))
}


case class Lt[+E](value: E) extends Etr[E, Nothing]
case class Rt[+A](value: A) extends Etr[Nothing, A]