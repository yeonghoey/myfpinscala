def mean(xs: Seq[Double]): Option[Double] = {
  if (xs.isEmpty) None
  else Some(xs.sum / xs.length)
}

mean(Seq(1, 2, 3))

def variance(xs: Seq[Double]): Option[Double] = {
  mean(xs).flatMap {
    m => mean(xs.map(x => math.pow(x - m, 2))) }
}

variance(Seq(1, 2, 3))
