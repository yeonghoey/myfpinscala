import sbt.Keys._

lazy val commonDependencies = Seq(
  "org.specs2" %% "specs2-core" % "3.6.5" % "test",
  "org.specs2" %% "specs2-scalacheck" % "3.6.5" % "test"
)

lazy val commonSettings = Seq(
  organization := "yeonghoey",

  version := "1.0",

  scalaVersion := "2.11.7",

  resolvers +=
    "Sonatype OSS Releases" at "https://oss.sonatype.org/service/local/staging/deploy/maven2",

  scalacOptions ++= Seq(
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions"),

  scalacOptions in Test ++= Seq(
    "-Yrangepos")
)

lazy val datastructures = (project in file("datastructures")).
  settings(commonSettings: _*).
  settings(
    name := "datastructures",
    libraryDependencies ++= commonDependencies)

lazy val errorhandling = (project in file("errorhadling")).
  settings(commonSettings: _*).
  settings(
    name := "errorhandling",
    libraryDependencies ++= commonDependencies)

lazy val laziness = (project in file("laziness")).
  settings(commonSettings: _*).
  settings(
    name := "laziness",
    libraryDependencies ++= commonDependencies)

lazy val state = (project in file("state")).
  settings(commonSettings: _*).
  settings(
    name := "state",
    libraryDependencies ++= commonDependencies)

lazy val parallelism = (project in file("parallelism")).
  settings(commonSettings: _*).
  settings(
    name := "parallelism",
    libraryDependencies ++= commonDependencies)

lazy val monads = (project in file("monads")).
  settings(commonSettings: _*).
  settings(
    name := "monads",
    libraryDependencies ++= commonDependencies)